#!/bin/bash
cpuinfo=$(cat /proc/cpuinfo | grep -e model\ name | sort -u | sed 's/model\ name//g;s/\://g' | awk '{$1=$1};1') #cpu name
cpucount=$(cat /proc/cpuinfo | grep -e CPU\ @ | wc -l)  #count cpu (hyperthreaded)cores
meminfo=$(cat /proc/meminfo | grep -e  'MemTotal' | sed 's/MemTotal\://g;s/kB//g') #memory
distro=$(cat /etc/os-release | grep -e 'PRETTY' | sed 's/PRETTY//g;s/\"//g;s/NAME=//g;s/\_//g') #distro name & info
upttime=$(uptime -p | sed 's/up//g') #gets uptime
packages="$(dpkg --list | wc -l)" #package count
shell="$(basename ${SHELL})" #shell name
#screenres=$(xdpyinfo | grep dimensions | sed 's/dimensions//g;s/\://g;s/(508x285//g;s/millimeters)//g;s/pixels//g') #get screen resolution
#battery=$(upower -i $(upower -e | grep '/battery') | grep --color=never -E percentage | xargs | cut -d' ' -f2) #battery %
((meminfo = meminfo / 1024)) #convert memory to MiB
#remove unneeded characters from cpu info
cpuinfo=${cpuinfo//(R)} 
cpuinfo=${cpuinfo//(TM)}
cpuinfo=${cpuinfo//Core}
cpuinfo=${cpuinfo//CPU}
cpuinfo=${cpuinfo//Intel}
cat <<EOF
     _____       Shell:    $shell
    /  __ \\      Uptime:  $upttime
   |  /    |     Distro:   $distro    
   |  \\___-      CPU:    $cpuinfo ($cpucount) 
    -_           Memory:   $meminfo MiB
      --_        Packages: $packages   
EOF